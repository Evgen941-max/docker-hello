ls
docker info
docker build -t docker-hellowrld:${BUILD_NUMBER} .
docker tag docker-hellowrld:${BUILD_NUMBER} docker-hellowrld:latest
docker run --name docker-hellowrld docker-hellowrld
docker logs docker-hellowrld